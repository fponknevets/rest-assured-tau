package utilities;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Iterator;

public class DataProviderSupplier {

    private JSONObject jsonObject;
    private Object[][] dataProvider;

    public DataProviderSupplier() {
        this(new JsonFileReader().getJSONObjectFromFilePath());
    }

    public DataProviderSupplier(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public Object[][] supplyDataProvider() {
        JSONArray records = (JSONArray) jsonObject.get("records");
        int numOfRecords = records.size();
        dataProvider = new Object[numOfRecords][3];
        int recordCounter = 0;
        for (Object o : records){
            JSONObject record = (JSONObject) o;
            dataProvider[recordCounter][0] = record.get("countryCode");
            dataProvider[recordCounter][1] = record.get("zipCode");
            dataProvider[recordCounter][2] = record.get("placeName");
            recordCounter++;
        }
        return dataProvider;
    }
}
