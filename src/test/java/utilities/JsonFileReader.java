package utilities;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;

/**
 * @author Crunchify.com
 * How to Read JSON Object From File in Java?
 */

public class JsonFileReader {

    private String path;
    private JSONParser parser;
    private Object obj;
    private JSONObject jsonObj;

    public JsonFileReader(){
        this("src/test/java/test-data/ZipCodeTestData.txt");
    }

    public JsonFileReader(String path) {
        this.path = path;
        parser = new JSONParser();
    }

    public JSONObject getJSONObjectFromFilePath() {
        try {
            obj = parser.parse(new FileReader(path));
            // A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
            jsonObj = (JSONObject) obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }
}
