package utilities;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.Iterator;

class JsonFileReaderTest {

    private static JsonFileReader jfr;
    private static JSONObject jObj;
    private static DataProviderSupplier dps;
    private static Object[][] dataProvider;

    public static void main(String[] args){

        dps = new DataProviderSupplier();
        dataProvider = dps.supplyDataProvider();
        for (Object[] record: dataProvider) {
            for (Object field: record) {
                System.out.println(field);
            }
        }

//        jfr = new JsonFileReader();
//        jObj = jfr.getJSONObjectFromFilePath();
//        System.out.println(jObj.toString());
//
//        JSONArray data = (JSONArray) jObj.get("testData");
//        System.out.println(data);
//        @SuppressWarnings("unchecked")
//        Iterator<JSONObject> it = data.iterator();
//        while(it.hasNext()){
//            JSONObject record = it.next();
//            System.out.println(record.get("zipCode"));
//        }
    }
}