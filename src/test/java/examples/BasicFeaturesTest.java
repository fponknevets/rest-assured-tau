package examples;

import io.restassured.http.ContentType;
import org.junit.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


// implement the BaseTest class but have not fully used it in this file
public class BasicFeaturesTest extends BaseTest {
    @Test public void requestZIPCode90210_checkStatusCode_expectHttp200(){
        given()
        .when()
            .get("https://zippopotam.us/us/90210")// don't need full url if implement requestSpecification
        .then()
            .assertThat()
            .statusCode(200); // don't need to do this if implement responseSpecification
    }
    @Test public void requestZIPCode90210_checkContentType_expectApplicationJson(){
        given()
        .when()
            .get("https://zippopotam.us/us/90210")
        .then()
            .assertThat()
            .contentType(ContentType.JSON);
    }
    /*
        Achieves exactly the same as the above two tests, but uses the RequestSpecification and
        ResponseSpecification to do parameterize the request and the assertions; this possible
        because the class extends BaseTest
    */
    @Test public void requestZipCode90210_useSpecs_to_CheckContentType_and_ResponseCode(){
        given()
            .when()
            .spec(requestSpec)
        .then()
            .spec(responseSpec);
    }
    @Test public void requestZIPCode90210_checkPlaceName_expectBeverlyHills(){
        given()
        .when()
            .get("https://zippopotam.us/us/90210")
        .then()
            .assertThat()
            .body("places[0].'place name'", equalTo("Beverly Hills"));
    }
    @Test public void requestZIPCode90210_logAllRequest_logResponseBody(){
        given().log().all()
        .when()
            .get("https://zippopotam.us/us/90210")
        .then().log().body();
    }
    @Test public void requestZIPCode90210_checkState_expectCalifornia(){
        given()
        .when()
            .get("https://zippopotam.us/us/90210")
        .then()
            .assertThat()
            .body("places[0].state", equalTo("California"));
    }
    @Test public void requestZIPCode90210_checkListOfStates_expectNotContainsNewYork(){
        given()
        .when()
            .get("https://zippopotam.us/us/90210")
        .then()
            .assertThat()
            .body("places.state", not(hasItem("New York")));
    }
    @Test public void requestZIPCode90210_checkSizeOfPlaceList_expect1(){
        given()
        .when()
            .get("https://zippopotam.us/us/90210")
        .then()
            .assertThat()
            .body("places.state", hasSize(1));
    }
}
