package examples;

import com.tngtech.java.junit.dataprovider.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import utilities.DataProviderSupplier;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@RunWith(DataProviderRunner.class)
public class UsingDataProvidersTest extends BaseTest {

    @DataProvider
    public static Object[][] zipCodesAndPlaces() {
        return new Object[][] {
                {"us", "90210", "Beverly Hills"},
                {"us", "12345", "Schenectady"},
                {"ca", "B2R", "Waverley"},
        };
    }

    // calls out to a utility function that reads data from file
    @DataProvider
    public static Object[][] zipCodesAndPlacesFromFile() {
        return new DataProviderSupplier().supplyDataProvider();
    }

    // test uses data provider specified in this file
    @Test
    @UseDataProvider("zipCodesAndPlaces")
    public void requestZipCodesFromDataProvider_checkPlaceNames_expectDataProviderValues(String countryCode, String zipCode, String expectedPlaceName) {
        given()
            .pathParam("countryCode", countryCode).pathParam("zipCode", zipCode)
        .when()
            .spec(requestSpec)
            .get("/{countryCode}/{zipCode}")
        .then()
            .spec(responseSpec)
            .assertThat()
            .body("places[0].'place name'", equalTo(expectedPlaceName));
    }

    // test uses data provider supplied by a utility function
    @Test
    @UseDataProvider("zipCodesAndPlacesFromFile")
    public void requestZipCodesFromDataProviderSupplier_checkPlaceNames_expectDataProviderValues(String countryCode, String zipCode, String expectedPlaceName) {
        given()
            .pathParam("countryCode", countryCode).pathParam("zipCode", zipCode)
        .when()
            .spec(requestSpec)
            .get("/{countryCode}/{zipCode}")
        .then()
            .spec(responseSpec)
            .assertThat()
            .body("places[0].'place name'", equalTo(expectedPlaceName));
    }
}
