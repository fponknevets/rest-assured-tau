package examples;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Assert;
import org.junit.Test;

public class SpecificationAndExtractionTest extends BaseTest {

    // uses the request and esponse specifications from the BaseTest class
    @Test
    public void demonstrateRequestAndResponseSpecificationReuse(){
        given()
            .spec(requestSpec)
        .when()
            .get("/us/90210")
        .then()
            .spec(responseSpec)
        .and()
            .assertThat()
            .body("places[0].'place name'", equalTo("Beverly Hills"));
    }

    @Test
    public void demonstrateValueExtraction(){
    // this technique good when you want to use the value from one api call
    // in a subsequent api call, e.g. getting an auth token
        String placeName =

        given()
            .spec(requestSpec)
        .when()
            .get("us/90210")
        .then()
            .extract()
            .path("places[0].'place name'");

        Assert.assertEquals(placeName, "Beverly Hills");
    }

}
