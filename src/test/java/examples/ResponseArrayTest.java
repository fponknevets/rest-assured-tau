package examples;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static io.restassured.RestAssured.given;

public class ResponseArrayTest extends BaseTest{

    @Test
    public void requestBN15_returnsFourPlacesTest() {

        ArrayList<LinkedHashMap> places =

        given()
            .spec(requestSpec)
        .when()
            .get("/gb/bn15")
        .then()
            .extract()
            .path("places");

        Assert.assertEquals(places.size(),4);
        Assert.assertEquals(places.get(0).get("place name"), "Coombes");
        Assert.assertEquals(places.get(1).get("place name"), "North Lancing");
        Assert.assertEquals(places.get(2).get("place name"), "Sompting");
        Assert.assertEquals(places.get(3).get("place name"), "South Lancing");
    }
}
