package examples;

import static io.restassured.RestAssured.*;

import org.junit.Assert;
import org.junit.Test;

import dataentities.Location;

public class SerialisationDeserialisationTest extends BaseTest {

    @Test public void deserialisation(){
        Location location =

        given()
            .spec(requestSpec)
        .when()
            .get("/us/90210")
            .as(Location.class);

        Assert.assertEquals(
            "Beverly Hills",
            location.getPlaces().get(0).getPlaceName());
    }
}
