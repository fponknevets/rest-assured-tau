# Notes from the [Introduction to REST Assured on TestAutomationU](https://testautomationu.applitools.com/automating-your-api-tests-with-rest-assured/) by Bas Dijkstra
These are the notes I took as I followed the course above.
<p>&nbsp;</p>

## SETUP
* Setup a Maven project in IntelliJ
* Make sure the SDK is 1.8 by

```Preferences -> Build, Execution, Deployment -> Java Compiler```

```File -> Project Structure -> Project Settings -> Project -> Java Version```

```Platform Settings -> SDKs```

Add properties to the pom.xml beofre the dependeincies section
```xml
 <properties> 
      <maven.compiler.source>1.8</maven.compiler.source> 
      <maven.compiler.target>1.8</maven.compiler.target> 
 </properties>
```
<p>&nbsp;</p>

## Hamcrest Matchers

https://hamcrest.org

<p>&nbsp;</p>

## JsonPath with GPath notation

https://groovy-lang.org/processing-xml.html#_gpath

https://www.james-willett.com/groovy-gpath-in-rest-assured-part1-overview

https://www.james-willett.com/rest-assured-gpath-json/

<p>&nbsp;</p>

## Learning Points
|Concept|Course Material|Example Code|Other Docs|
|--|---|--|--|
|Hamcrest Matchers<br />Status Code checking<br />Content type checking<br />Request and response loging|[Chapter 2](https://testautomationu.applitools.com/automating-your-api-tests-with-rest-assured/chapter2.html)|[BasicFeaturesTest](./src/test/java/examples/BasicFeaturesTest.java)||
|DataProviders|[Chapter 3](https://testautomationu.applitools.com/automating-your-api-tests-with-rest-assured/chapter3.html)|[UsingDataProvidersTest](./src/test/java/examples/UsingDataProvidersTest.java)<br />[DataProviderSupplier](./src/test/java/utilities/DataProviderSupplier.java)|[JUnit DataProvider](https://github.com/TNG/junit-dataprovider)|
|Testing values of embedded JSONArray||[SpecificationAndExtractionTest](./src/test/java/examples/SpecificationAndExtractionTest.java)||
|Request Specification, Response Specifications and value extraction|[Chapter 4](https://testautomationu.applitools.com/automating-your-api-tests-with-rest-assured/chapter4.html)|[SpecificationAndExtractionTest](./src/test/java/examples/SpecificationAndExtractionTest.java)|[Specification Reuse](https://github.com/rest-assured/rest-assured/wiki/Usage#specification-re-use)|

<p>&nbsp;</p>

## Other Resources
http://teamaqua.github.io/JSONTest/

<p>&nbsp;</p>

## Postman API Key

```
PMAK-5ee5e36834b22e004df75b0b-694078f4d19f51c8af4f4f3fec640569a3
```



